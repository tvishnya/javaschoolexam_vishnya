package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

	/**
	 * Builds a pyramid with sorted values (with minumum value at the top line and
	 * maximum at the bottom, from left to right). All vacant positions in the array
	 * are zeros.
	 *
	 * @param inputNumbers to be used in the pyramid
	 * @return 2d array with pyramid inside
	 * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build
	 *                with given input
	 */
	public int[][] buildPyramid(List<Integer> inputNumbers) {
		if (inputNumbers == null || inputNumbers.contains(null)) {
			throw new CannotBuildPyramidException();
		}
		int sz = inputNumbers.size();
		int vSize;
		for (vSize = 1; sz > 0; vSize++) {
			sz -= vSize;
		}
		if (sz < 0) {
			throw new CannotBuildPyramidException();
		}

		vSize--;
		Collections.sort(inputNumbers);// this corrupts the inputNumbers
		int hSize = vSize * 2 - 1;
		int[][] result = new int[vSize][hSize];
		for (int i = 0; i < vSize; i++) {
			for (int j = 0; j < hSize; j++) {
				result[i][j] = 0;
			}
		}
		int leftBorder = vSize - 1;
		int rightBorder = vSize - 1;
		int odd = leftBorder % 2;
		int numElem = 0;
		for (int i = 0; i < vSize; i++) {
			for (int j = 0; j < hSize; j++) {
				if (j >= leftBorder && j <= rightBorder && (j % 2 == odd)) {
					result[i][j] = inputNumbers.get(numElem);
					numElem++;
				} else {
					result[i][j] = 0;
				}
			}
			leftBorder--;
			rightBorder++;
			odd = leftBorder % 2;
		}
		return result;
	}

}
