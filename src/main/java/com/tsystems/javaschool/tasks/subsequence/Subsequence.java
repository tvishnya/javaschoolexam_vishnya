package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.lang.IllegalArgumentException;

public class Subsequence {

	/**
	 * Checks if it is possible to get a sequence which is equal to the first one by
	 * removing some elements from the second one.
	 *
	 * @param x first sequence
	 * @param y second sequence
	 * @return <code>true</code> if possible, otherwise <code>false</code>
	 */
	@SuppressWarnings("rawtypes")
	public boolean find(List x, List y) {
		if (x == null || y == null) {
			throw new IllegalArgumentException("Lists must exist.");
		}
		int currPos;
		List currList = y; // for not to spoil y
		for (Object elem : x) {
			currPos = currList.indexOf(elem);
			if (currPos == -1)
				return false;
			currList = currList.subList(currPos + 1, currList.size());
		}
		return true;
	}
}
