package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.HashMap;

public class Expression {
	ArrayList<Token> elements;

	public Expression(String expression) {
		elements = splitToElements(expression);

	}

	private String evaluate(List<Token> elements2) {

		if (elements2 == null)
			return null;
		if (elements2.isEmpty())
			return null;
		String result = "";

		// this program does not work with expressions with leading '-' and '+' as there
		// were no such tests )))

		boolean done = true;
		boolean brackets = checkBrackets(elements2);
		while (brackets) { // if there are brackets then calculate and replace
			done = calcGroup(elements2); // evaluate
			if (!done)
				return null;
			brackets = checkBrackets(elements2);
		}

		if (elements2.size() > 3) {
			int priorityPos = getPriority(elements2);
			elements2.add(priorityPos - 1, new Token("(", ElementTypes.OPENGROUP));
			elements2.add(priorityPos + 3, new Token(")", ElementTypes.CLOSEGROUP));
			result = evaluate(elements2);
			return result;
		}
		if (elements2.size() == 3) {
			result = evaluate3(elements2);
			if (result == null)
				return null;
			Token newElem = new Token(result, ElementTypes.NUMBER);
			elements2.clear();
			elements2.add(newElem);
			return result;
		}
		if (elements2.size() == 1) {
			result = elements2.get(0).getValue();
		}

		return result;
	}

	private int getPriority(List<Token> elements2) {
		int priorityPos = -1;
		boolean first = true;

		for (int i = 0; i < elements2.size(); i++) {
			Token elem = elements2.get(i);
			ElementTypes elemType = elem.getType();
			if (elemType == ElementTypes.OPERATION) {
				int currPriority = elemPriority(elem.getValue());

				if (first) {
					if (currPriority == 1) {
						first = false;
						priorityPos = i;
					}
				}
				if (currPriority == 2) {
					return i;
				}
			}
		}
		return priorityPos;
	}

	private int elemPriority(String opName) {
		if (opName.contentEquals("+") || opName.contentEquals("-"))
			return 1;
		if (opName.contentEquals("*") || opName.contentEquals("/"))
			return 2;
		return 0;
	}

	private boolean calcGroup(List<Token> elements2) {
		int startPos = -1;
		int endPos = -1;
		boolean result = true;
		boolean found = false;
		List<Token> group = new ArrayList<Token>();
		String groupResult;
		for (int i = 0; i < elements2.size(); i++) {
			Token currElem = elements2.get(i);
			ElementTypes currType = currElem.getType();
			if (currType == ElementTypes.OPENGROUP) {
				startPos = i;
			}
			if (currType == ElementTypes.CLOSEGROUP) {
				endPos = i;
				found = true;
			}
			if (found) {
				group = elements2.subList(startPos + 1, endPos);
				groupResult = evaluate(group);
				replaceGroup(elements2, groupResult, startPos, startPos + 3);
				break;
			}
		}

		return result;
	}

	private void replaceGroup(List<Token> elements2, String groupResult, int startPos, int endPos) {
		for (int j = startPos; j < endPos; j++) {// replace

			elements2.remove(startPos);
		}
		Token newElem = new Token(groupResult, ElementTypes.NUMBER);

		elements2.add(startPos, newElem);

	}

	private boolean checkBrackets(List<Token> elements2) {
		// true if found
		for (Token t : elements2) {
			if (t.getType() == ElementTypes.OPENGROUP || t.getType() == ElementTypes.CLOSEGROUP)
				return true;
		}
		return false;
	}

	public String evaluate() {
		return evaluate(elements);

	}

	private String evaluate3(List<Token> elements2) {

		Token elem0 = elements2.get(0);
		Token elem1 = elements2.get(1);
		Token elem2 = elements2.get(2);

		if (elem0.getType() != ElementTypes.NUMBER || elem1.getType() != ElementTypes.OPERATION
				|| elem2.getType() != ElementTypes.NUMBER) {
			return null;
		}

		String result;
		try {

			double e0 = Double.parseDouble(elem0.getValue());
			double e2 = Double.parseDouble(elem2.getValue());
			char operation = (elem1.getValue()).charAt(0);
			double resultD = 0;
			switch (operation) {
			case '+':
				resultD = e0 + e2;
				break;
			case '-':
				resultD = e0 - e2;
				break;
			case '*':
				resultD = e0 * e2;
				break;
			case '/':
				if (e2 == 0)
					throw new ArithmeticException();
				resultD = e0 / e2;
				break;
			}
			result = String.format(Locale.US, "%f", resultD);
			return result;
		} catch (Exception e) {
			return null;
		}

	}

	private ArrayList<Token> splitToElements(String expression) {
		StringBuilder currElem = new StringBuilder();
		ArrayList<Token> result = new ArrayList<Token>();
		ElementTypes currType = ElementTypes.UNKNOWN, prevType = ElementTypes.UNKNOWN;
		HashMap<Character, ElementTypes> types = createTypesHash();
		boolean first = true;
		boolean append = false;
		for (int i = 0; i < expression.length(); i++) {
			char currChar = expression.charAt(i);
			currType = types.get(currChar);
			if (first) {
				currElem.append(currChar);
				first = false;
			} else {
				if (currType == prevType) {
					if (currType == ElementTypes.OPERATION)
						return null;
					if (currType == ElementTypes.NUMBER) {
						currElem.append(currChar);
					}

					else {
						append = true;
					}
				} else {

					append = true;
				}

				if (append) {
					Token elem = new Token(currElem.toString(), prevType);
					if (!elem.check())
						return null;
					result.add(elem);
					currElem = new StringBuilder();
					currElem.append(currChar);
					append = false;
				}
			}
			prevType = currType;
			boolean last = false;
			;
			if (i == (expression.length() - 1))
				last = true;
			if (last) {
				Token elem = new Token(currElem.toString(), currType);
				if (!elem.check())
					return null;
				result.add(elem);
			}

		}
		return result;
	}

	private HashMap<Character, ElementTypes> createTypesHash() {
		HashMap<Character, ElementTypes> types = new HashMap<>();
		String numbers = ".0123456789";
		String operations = "/+-*";
		for (char currChar : numbers.toCharArray()) {
			types.put(currChar, ElementTypes.NUMBER);
		}
		for (char currChar : operations.toCharArray()) {
			types.put(currChar, ElementTypes.OPERATION);
		}
		types.put('(', ElementTypes.OPENGROUP);
		types.put(')', ElementTypes.CLOSEGROUP);
		return types;
	}

	public boolean ifLegal() {
		if (elements == null)
			return false;
		return true;
	}

}
