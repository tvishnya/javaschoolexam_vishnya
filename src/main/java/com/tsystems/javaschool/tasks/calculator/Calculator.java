package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Calculator {

	/**
	 * Evaluate statement represented as string.
	 *
	 * @param statement mathematical statement containing digits, '.' (dot) as
	 *                  decimal mark, parentheses, operations signs '+', '-', '*',
	 *                  '/'<br>
	 *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
	 * @return string value containing result of evaluation or null if statement is
	 *         invalid
	 */
	public String evaluate(String statement) {

		if (!checkLegality(statement))
			return null;
		Expression expression = new Expression(statement);
		if (!expression.ifLegal())
			return null;

		String result = expression.evaluate();
		if (result == null)
			return null;
		int dotPos = result.indexOf('.');
		if (dotPos != -1) {
			double d = Double.parseDouble(result);
			DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
			df.setMaximumFractionDigits(4);
			result = String.format(Locale.US, df.format(d));
		}
		return result;
	}

	private boolean checkLegality(String input) {
		if (input == null)
			return false;
		if (!checkLegalSymbols(input))
			return false;
		if (!checkBrackets(input))
			return false;
		if (input.isEmpty())
			return false;
		return true;
	}

	private boolean checkBrackets(String input) {
		// count opening and closing from the beginning
		// if at any time closing > opening -> this is bad news
		char op = '(';
		char clo = ')';
		int numOpening = 0;
		int numClosing = 0;
		for (char currChar : input.toCharArray()) {
			if (currChar == op)
				numOpening++;
			if (currChar == clo)
				numClosing++;
			if (numClosing > numOpening)
				return false;
		}
		if (numOpening != numClosing)
			return false;
		return true;
	}

	private boolean checkLegalSymbols(String input) {
		String legal = "0123456789()+-* ./";
		boolean result = true;
		for (char currChar : input.toCharArray()) {

			if (legal.indexOf(currChar) == -1) {
				result = false;
			}
		}
		return result;
	}

}
