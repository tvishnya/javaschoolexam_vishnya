package com.tsystems.javaschool.tasks.calculator;

public enum ElementTypes {
NUMBER,  OPERATION, OPENGROUP, CLOSEGROUP, UNKNOWN, INCORRECT
}
