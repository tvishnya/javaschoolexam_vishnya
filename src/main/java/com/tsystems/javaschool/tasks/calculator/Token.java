package com.tsystems.javaschool.tasks.calculator;

public class Token {
	ElementTypes type;
	// possible types: num (including dot), space , (, ), action
	String value;
	double valueD;
	boolean counted;

	public Token(String v, ElementTypes t) {
		type = t;
		value = v;
	}

	public ElementTypes getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	public boolean check() {
		// check if the number has 2 dots in it
		if (type == ElementTypes.NUMBER) {
			int dotCount = 0;
			for (char ch : value.toCharArray()) {
				if (ch == '.')
					dotCount++;
			}
			if (dotCount > 1)
				return false;
		}
		return true;
	}
}
